package Tetris;

import java.awt.*;

public class Model {

    public static View view = new View(Glass.WIDTH_FIELD, Glass.HEIGHT_FIELD, Block.SIDE) ;

    static void blockPaint(Figur object, Graphics g, int tm){          // метод для получения координат на поле, связывает Figur и View
        int i=0, b=0, x, y;
        int[][] x_c;
        int[][] y_c;
        x_c = object.getCoordinatesX();
        y_c = object.getCoordinatesY();
        while (i<4){
            while(b<4) {
                x = x_c[i][b];
                y = y_c[i][b];

                if (tm==0) {                                        // tm==0 нарисовать фигуру
                    if(object.matrix_F[i][b]!=0)
                        view.Paint(g, x, y, object.color);
                }
                if(tm==1){                                          // tm==1 удалить фигуру
                    if(object.matrix_F[i][b]!=0)
                        view.deleteOldObject(g, x, y);
                }

                b++;
            }
            b=0;
            i++;
        }
    }

    static void paintLine(int x, int y, Color color) {
        Graphics g;
        g = view.Graph();
        view.Paint(g, x, y, color);
    }

    static Graphics getGraph(){
        Graphics g = view.Graph();
        return g;
    }

    static void repaintScore(Graphics g, int Game_score){
        String Score_game;
        Score_game = Integer.toString ( Game_score);

        view.scorePaint(g, Score_game);
    }

}

package Tetris;

import java.awt.*;
import java.util.Random;

public class Figur {

    int[][] matrix_F = new int[4][4];
    Color color;
    int[][] x_arr = new int[4][4];
    int[][] y_arr = new int[4][4];
    int PointX;
    int PointY;  // PointX - точка x в матричных координатах, вокруг которой будет вращаться фигура, добавлено, чтобы фигура не "улетала" при вращении

    // методы взаимодействия

    void stepDown(Glass grd) {
        int i = 0, k = 0;

        while (i < 4) {
            while (k < 4) {
                if (y_arr[i][k] >= 0)
                    grd.glass[y_arr[i][k]][x_arr[i][k]] = 0;// обнуляем элементы матрицы стакана по координатам в массивах координат x и y
                if (matrix_F[i][k] != 0) {
                    y_arr[i][k] = y_arr[i][k] + 1;             // добавляем к каждому элементу массива координат y +1
                    grd.glass[y_arr[i][k]][x_arr[i][k]] = 1; // Записываем в массив стакана единицы на строчку ниже
                }
                k++;
            }
            i++;
            k = 0;
        }

    }

    boolean isTouchDown(Glass grd) {
        boolean result = false;


        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                if (y_arr[i][k] != 23) {
                    if (grd.glass[y_arr[i][k] + 1][x_arr[i][k]] == 2) {
                        result = true;
                        break;
                    }
                } else {
                    result = true;
                    break;
                }
            }
        }

        return result;

    }

    void liveOnFloor(Glass grd) {
        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                if (matrix_F[i][k] != 0)
                    grd.glass[y_arr[i][k]][x_arr[i][k]] = 2;
                grd.glass_col[y_arr[i][k]][x_arr[i][k]] = color;
            }
        }
    }

    void isLeft(Glass grd) {
        int i = 0, k = 0;

        while (i < 4) {
            while (k < 4) {
                grd.glass[y_arr[i][k]][x_arr[i][k]] = 0;// обнуляем элементы матрицы стакана по координатам в массивах координат x и y
                if (matrix_F[i][k] != 0) {
                    x_arr[i][k] = x_arr[i][k] - 1;             // добавляем к каждому элементу массива координат x -1
                    grd.glass[y_arr[i][k]][x_arr[i][k]] = 1; // Записываем в массив стакана единицы на столбец левее
                }
                k++;
            }
            i++;
            k = 0;
        }
        PointX = PointX - 1;


    }

    void isRight(Glass grd) {
        int i = 0, k = 0;

        while (i < 4) {
            while (k < 4) {
                grd.glass[y_arr[i][k]][x_arr[i][k]] = 0;// обнуляем элементы матрицы стакана по координатам в массивах координат x и y
                if (matrix_F[i][k] != 0) {
                    x_arr[i][k] = x_arr[i][k] + 1;             // добавляем к каждому элементу массива координат x -1
                    grd.glass[y_arr[i][k]][x_arr[i][k]] = 1; // Записываем в массив стакана единицы на столбец левее
                }
                k++;
            }
            i++;
            k = 0;
        }
        PointX = PointX + 1;

    }

    void isDrop(Glass grd) {
        int i = 0, k = 0;
        boolean pr = false;

        while (i < 4) {
            while (k < 4) {
                grd.glass[y_arr[i][k]][x_arr[i][k]] = 0;// обнуляем элементы матрицы стакана по координатам в массивах координат x и y
                if (matrix_F[i][k] != 0) {
                    y_arr[i][k] = y_arr[i][k] + 3;             // добавляем к каждому элементу массива координат y +1
                    grd.glass[y_arr[i][k]][x_arr[i][k]] = 1; // Записываем в массив стакана единицы на строчку ниже
                }
                k++;
            }
            if (pr == true)
                break;
            i++;
            k = 0;
        }
    }

    void transform(int key) {

        int[][] matrix_4x4 = new int[4][4];

        getPointXY(key);

        if (key == 4) {
            correct(matrix_F);
            key = 0;
        }

        switch (key) {

            case 0: {                                             // Первый поворот на 90;

                transpose(matrix_F, matrix_4x4);

                int n = 3, m = 3;
                for (int k = 0; k < 4; k++) {                       // записываем столбцы в обратном порядке
                    for (int i = 0; i < 4; i++) {
                        matrix_F[m][n] = 0;
                        matrix_F[m][n] = matrix_4x4[i][k];

                        m--;
                    }
                    n--;
                    m = 3;
                }

                getNewCoordinate(PointY);
                break;
            }
            case 1: {
                int n = 3;
                for (int i = 0; i < 4; i++) {
                    for (int k = 0; k < 4; k++) {
                        matrix_4x4[i][k] = matrix_F[i][n];      // записываем значения строк в обратном порядке
                        n--;
                    }
                    n = 3;
                }

                for (int i = 0; i < 4; i++) {
                    for (int k = 0; k < 4; k++) {
                        matrix_F[i][k] = 0;
                        matrix_F[i][k] = matrix_4x4[k][i];        // транспонируем
                        x_arr[i][k] = 0;
                        y_arr[i][k] = 0;
                        x_arr[i][k] = matrix_F[i][k] * (PointX + k);
                        y_arr[i][k] = matrix_F[i][k] * (PointY - i + 1);
                    }
                }

                break;
            }
            case 2: {
                transpose(matrix_F, matrix_4x4);
                int n = 3;


                for (int i = 0; i < 4; i++) {
                    for (int k = 0; k < 4; k++) {
                        matrix_F[i][k] = matrix_4x4[n][k];      // записываем значения в столбцах в обратном порядке
                    }
                    n--;
                }


                getNewCoordinate(PointY);

                break;
            }
            case 3: {
                transpose(matrix_F, matrix_4x4);

                int n = 3;

                for (int i = 0; i < 4; i++) {
                    for (int k = 0; k < 4; k++) {
                        matrix_F[i][k] = matrix_4x4[n][k];      // записываем значения в столбцах в обратном порядке
                    }
                    n--;
                }


                for (int k = 0; k < 4; k++) {
                    matrix_F[0][k] = matrix_F[2][k];
                    matrix_F[1][k] = matrix_F[3][k];
                    matrix_F[2][k] = 0;
                    matrix_F[3][k] = 0;
                }


                getNewCoordinate(PointY);


                break;

            }
        }
    }

    boolean Left(Glass grd) {

        boolean result = true;


        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                if (matrix_F[i][k] != 0) {

                    try {
                        if (x_arr[i][k] - 1 >= 0 && grd.glass[y_arr[i][k]][x_arr[i][k] - 1] != 2
                                && grd.glass[y_arr[i][k] + 1][x_arr[i][k] - 1] != 2) {
                            result = true;
                        } else {
                            result = false;
                            break;
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        result = false;
                        break;
                    }
                }
            }
            if (result == false)
                break;
        }
        return result;
    }

    boolean Right(Glass grd) {

        boolean result = true;


        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                if (matrix_F[i][k] != 0) {

                    try {
                        if (x_arr[i][k] + 1 >= 0 && grd.glass[y_arr[i][k]][x_arr[i][k] + 1] != 2
                                && grd.glass[y_arr[i][k] + 1][x_arr[i][k] + 1] != 2) {
                            result = true;
                        } else {
                            result = false;
                            break;
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        result = false;
                        break;
                    }
                }
            }
            if (result == false)
                break;
        }
        return result;
    }

    boolean Drop(Glass grd) {

        boolean result = true;


        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                if (matrix_F[i][k] != 0) {

                    try {
                        for (int n = 0; n <= 4; n++) {
                            if (y_arr[i][k] + 3 <= grd.y_s && grd.glass[y_arr[i][k] + n][x_arr[i][k]] != 2) {
                                result = true;
                            } else {
                                result = false;
                                break;
                            }
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        result = false;
                        break;
                    }
                }
            }
            if (result == false)
                break;
        }
        return result;
    }

    boolean transformCheck(Glass grd) {
        int x = 0, y = 0;
        boolean pr = false;
        boolean result = true;

        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                if (pr == false) {
                    if (matrix_F[i][k] != 0) {
                        x = x_arr[i][k];
                        y = y_arr[i][k];
                        pr = true;
                        break;
                    }
                }
            }
            if (pr == true)
                break;
        }

        for (int m = 0; m <= 3; m++) {
            for (int n = 0; n <= 3; n++) {
                try {
                    if (grd.glass[y + m][x + n] != 2 && grd.glass[y + m][x + n] >= 0 && x + n <= grd.x_s
                            && y > 0 && x - 1 >= 0) {
                        result = true;
                    } else {
                        result = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    result = false;
                    break;
                }
            }
            if (result == false)
                break;
        }


        return result;
    }


    // служебные методы (удалить объект, получить координаты в графическом формате)

    int[][] getCoordinatesX() {

        int[][] x = new int[4][4];
        for (int k = 0; k < 4; k++) {
            for (int i = 0; i < 4; i++) {
                if (matrix_F[k][i] != 0)
                    x[k][i] = x_arr[k][i] * Block.SIDE;
            }
        }
        return x;
    }

    int[][] getCoordinatesY() {

        int[][] y = new int[4][4];
        for (int k = 0; k < 4; k++) {
            for (int i = 0; i < 4; i++) {
                if (matrix_F[k][i] != 0)
                    y[k][i] = y_arr[k][i] * Block.SIDE;
            }
        }
        return y;
    }

    void DeleteObject() {
        for (int k = 0; k < 4; k++) {
            for (int i = 0; i < 4; i++) {
                x_arr[k][i] = 0;
                y_arr[k][i] = 0;
                matrix_F[k][i] = 0;
            }
        }
    }

    // приватные методы для транспонирования, получения матричных координат и т.д.

    private void correct(int[][] matrix_F) {
        int[][] matrix_4x4 = new int[4][4];

        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                matrix_4x4[i][k] = matrix_F[i][k];
            }
        }


        for (int k = 0; k < 4; k++) {
            matrix_F[1][k] = matrix_4x4[0][k];
            matrix_F[0][k] = matrix_4x4[1][k];
        }
    }

    private void transpose(int[][] matrix_F, int[][] matrix_4x4) {
        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                matrix_4x4[i][k] = matrix_F[k][i];        // транспонируем
            }
        }
    }

    private void getNewCoordinate(int PointY) {
        for (int i = 0; i < 4; i++) {
            for (int k = 0; k < 4; k++) {
                x_arr[i][k] = 0;
                y_arr[i][k] = 0;
                x_arr[i][k] = matrix_F[i][k] * (PointX + k);
                y_arr[i][k] = matrix_F[i][k] * (PointY - i + 1);
            }
        }
    }

    private void getPointXY(int key) {

        for (int i = 0; i < 4; i++) {                   // вычисляем точку опоры, первая попавшаяся координата в матрице с фигурой, у которой есть координаты. Отсчет с нулевой строчки матрицы.
            for (int k = 0; k < 4; k++) {
                if (x_arr[i][k] != 0) {
                    if (key == 0) {
                        PointX = x_arr[i][k];
                    }
                    PointY = y_arr[i][k];
                    break;
                }
            }
        }
    }

    private int RandomSize() {
        Random random = new Random();
        int num = 1 + random.nextInt(7);
        return num;
    }

    // конструктор, в котором задается матрица фигуры

    public Figur() {
        int[] rect_1 = new int[4];
        int[] rect_2 = new int[4];
        int rnd;
        rnd = RandomSize();

        switch (rnd) {
            case (1):
                int[] rect_3 = {1, 1, 1, 1};
                rect_1 = rect_3;
                break;
            case (2):
                int[] rect_4 = {1, 1, 1, 0};
                rect_1 = rect_4;
                break;
            case (3):
                int[] rect_5 = {1, 1, 1, 0};
                rect_1 = rect_5;
                break;
            case (4):
                int[] rect_6 = {0, 1, 1, 0};
                rect_1 = rect_6;
                break;
            case (5):
                int[] rect_7 = {1, 1, 0, 0};
                rect_1 = rect_7;
                break;
            case (6):
                int[] rect_8 = {0, 1, 1, 0};
                rect_1 = rect_8;
                break;
            case (7):
                int[] rect_9 = {0, 1, 1, 1};
                rect_1 = rect_9;
                break;
        }
        switch (rnd) {
            case (1):
                int[] type_1 = {0, 0, 0, 0};
                Color c1 = new Color(5, 225, 255);
                rect_2 = type_1;
                color = c1;
                break;
            case (2):
                int[] type_2 = {1, 0, 0, 0};
                Color c2 = Color.BLUE;
                color = c2;
                rect_2 = type_2;
                break;
            case (3):
                int[] type_3 = {0, 0, 1, 0};
                Color c3 = new Color(255, 118, 0);
                color = c3;
                rect_2 = type_3;
                break;
            case (4):
                int[] type_4 = {0, 1, 1, 0};
                color = Color.YELLOW;
                rect_2 = type_4;
                break;
            case (5):
                int[] type_5 = {0, 1, 1, 0};
                color = Color.GREEN;
                rect_2 = type_5;
                break;
            case (6):
                int[] type_6 = {1, 1, 0, 0};
                color = Color.RED;
                rect_2 = type_6;
                break;
            case (7):
                int[] type_7 = {0, 0, 1, 0};
                Color c4 = new Color(201, 0, 255);
                color = c4;
                rect_2 = type_7;
                break;

        }

        for (int i = 0; i < 4; i++) {
            matrix_F[1][i] = rect_1[i];
            matrix_F[0][i] = rect_2[i];
        }

    }

}

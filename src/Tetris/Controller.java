package Tetris;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

//Контроллер

public class Controller {
    private static GameLogic NewGame = new GameLogic();
    static EnumMotions enumMotions;


    static EnumMotions Listener(){
        Model.view.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed (KeyEvent e) {
                if (NewGame.checkGameOver() == false) {
                    int k = e.getKeyCode();
                    switch(k) {
                        case (KeyEvent.VK_LEFT): {
                            enumMotions = enumMotions.Left; break;
                        }
                        case (KeyEvent.VK_RIGHT): {
                            enumMotions = enumMotions.Right; break;
                        }
                        case (KeyEvent.VK_DOWN): {
                            enumMotions = enumMotions.Down; break;
                        }
                        case (KeyEvent.VK_UP): {
                            enumMotions = enumMotions.Transform; break;
                        }
                        default: {
                            enumMotions=enumMotions.Null; break;
                        }
                    }
                }

            }
        });
        if (enumMotions==null){
            enumMotions=enumMotions.Null;
        }

        return enumMotions;
    }


    static void GameOver(int Game_Score){
        String Score_game;
        Score_game = Integer.toString ( Game_Score);

        Model.view.paintGameOver(Score_game);
    }

    Controller(GameLogic NewGame){
        this.NewGame=NewGame;
    }

}


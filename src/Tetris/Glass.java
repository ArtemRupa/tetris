package Tetris;

import java.awt.*;

public class Glass {


    // Логический стакан, представлен ввиде матрицы, объекты в нем также представлены в виде матрицы.

    public static final int WIDTH_FIELD = 400;
    public static final int HEIGHT_FIELD = 600;
    public int x_s=WIDTH_FIELD/Block.SIDE; // 16
    int y_s=HEIGHT_FIELD/Block.SIDE; // 24
    int [][] glass;
    Color [][] glass_col;
    public static int line_y=0;


    public void addFigur(Figur object){

        for(int k=0; k<4; k++){
            for (int i=0;i<4; i++){
                glass[k][6+i]=glass[k][6+i]+object.matrix_F[k][i];
                if(glass[k][6+i]!=0) {
                    if(object.matrix_F[k][i]!=0) {
                        object.x_arr[k][i] = (6 + i);
                        object.y_arr[k][i] = k;
                    }
                }
            }
        }

    }   //добавить фигурку в матрицу стакана
    public int fullLine(){
        boolean res=false;
        int y=0;


        for(int i=0; i<y_s; i++){
            for (int k=0;k<x_s; k++) {
                if(glass[i][k]==2){
                    res=true;
                } else{
                    res=false;
                    break;
                }
            }
            if(res==true) {
                y=i;
                break;
            }
        }

        if(res==true) {
            for (int k = 0; k < x_s; k++) {
                glass[y][k] = 0;
            }
            line_y=y;
        } else {
            line_y=0;
        }
        return line_y;

    }           // проверка на заполненность линии
    public void newLine(int y){           //удаляем полную линию в матрице и опускаем остальные блоки в стакане на -1

        Color color;

        for(int i=y; i>0; i--) {
            for (int k = 0; k < x_s; k++) {
                if(i-1>0) {
                    color=glass_col[i-1][k];
                    glass_col[i][k]=glass_col[i-1][k];
                    glass[i][k] = glass[i - 1][k];
                    Model.paintLine(k*Block.SIDE, i*Block.SIDE, color);
                }
            }
        }
    }       //

    public Glass(){
        glass = new int[y_s][x_s];
        glass_col = new Color[y_s][x_s];
        for(int i=0; i<y_s; i++){
            for(int k=0; k<x_s; k++) {
                glass[i][k]=0;
                glass_col[i][k]=Color.BLACK;
            }
        }
    }
}

package Tetris;

import java.awt.*;


public class GameLogic {

    Figur object = new Figur();
    int k = 0;
    Glass Glass = new Glass();
    int GameScore = 0;
    EnumMotions enumMotions;


    // Игровая логика

    public void GameProcess() {
        Graphics g;
        Glass.addFigur(object);

        while (checkGameOver() == false) {
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (object.isTouchDown(Glass) == false) {
                g = Model.getGraph();
                Model.repaintScore(g, GameScore);
                Model.blockPaint(object, g, 1);
                buttonLiest(Controller.Listener());
                Controller.enumMotions = enumMotions.Null;
                object.stepDown(Glass);
            } else {
                object.liveOnFloor(Glass);
                fullLine();
                g = Model.getGraph();
                GameScore = GameScore + 10;
                Model.repaintScore(g, GameScore);
                if (checkGameOver() == false) {
                    object.DeleteObject();
                    Figur new_object = new Figur();
                    k = 0;
                    object = new_object;
                    g = Model.getGraph();
                    Glass.addFigur(object);
                    Model.blockPaint(object, g, 1);
                } else {
                    break;
                }
            }
            Model.blockPaint(object, g, 0);
        }
    }

    private void fullLine() {
        int y, n = 0;
        while (n < 4) {
            y = Glass.fullLine();
            if (y != 0) {
                {
                    Glass.newLine(y);
                    GameScore = GameScore + 100;
                }
            }
            n++;
        }
    }           // вызов Ground на проверку, не заполнилась ли какая-то линия после того, как фигура зафиксирована

    private void buttonLiest(EnumMotions enumMotions) {
        boolean res;
        switch (enumMotions) {
            case Left: {

                Model.blockPaint(object, Model.getGraph(), 1);
                res = object.Left(Glass);
                if (res == true) {
                    object.isLeft(Glass);
                }
                break;
            }
            case Right: {

                Model.blockPaint(object, Model.getGraph(), 1);
                res = object.Right(Glass);
                if (res == true) {
                    object.isRight(Glass);
                }
                break;
            }
            case Down: {

                Model.blockPaint(object, Model.getGraph(), 1);
                res = object.Drop(Glass);
                if (res == true) {
                    object.isDrop(Glass);
                }
                break;
            }
            case Transform: {
                if (object.color != Color.YELLOW) {
                    Model.blockPaint(object, Model.getGraph(), 1);
                    res = object.transformCheck(Glass);
                    if (res == true) {
                        if (k == 5) {
                            k = 1;
                        }
                        object.transform(k);
                        k++;
                    }
                }
                break;
            }
            case Null:
                break;
            default:
                break;
        }
    }       //обработчик нажатий лево-право

    public boolean checkGameOver() {
        boolean pr = false;

        for (int k = 0; k < Glass.x_s; k++) {
            if (Glass.glass[0][k] == 2) {
                pr = true;
                Controller.GameOver(GameScore);
                break;
            }
        }

        return pr;
    }


}


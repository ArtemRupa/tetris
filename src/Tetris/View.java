
package Tetris;
import java.awt.*;
import javax.swing.*;


public class View extends JFrame {

    int SIDE;

    public void Paint(Graphics g, int x, int y, Color color)    // отрисовка блока
    {
        g.drawRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.fillRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.setColor(color);

        g.drawRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.fillRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.setColor(color);


}

    public Graphics Graph() // получить графику
    {
        Graphics g = getGraphics();
        return g;
    }


    public void deleteOldObject(Graphics g, int x, int y) //закрашивание предыдущего блока (при шаге вниз или вправо/влево)
    {
        g.drawRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.fillRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.setColor(Color.BLACK);

        g.drawRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.fillRoundRect(x, y, SIDE, SIDE, 10, 10);
        g.setColor(Color.BLACK);

    }

    public void scorePaint(Graphics g, String Game_Score){
        scoreDelete(g);
        g.drawString(Game_Score, 325, 50);
        g.setColor(Color.WHITE);
        g.drawString(Game_Score, 325, 50);
        g.setColor(Color.WHITE);
    }
    private void scoreDelete(Graphics g) {
        g.drawRect(325, 25, 100,25);
        g.fillRect(325, 25, 100, 25);
        g.setColor(Color.BLACK);

        g.drawRect(325, 25, 100,25);
        g.fillRect(325, 25, 100, 25);
        g.setColor(Color.BLACK);
    }

    public void paintGameOver(String Game_Score){
        Font f = new Font("Roboto", Font.BOLD, 48);
        setFont(f);
        Graphics g = Graph();

        g.drawString("GAME OVER", 50, 250);
        g.setColor(Color.red);
        g.drawString("GAME OVER", 50, 250);
        g.setColor(Color.red);

        g.drawString("Your score:", 70, 350);
        g.setColor(Color.WHITE);
        g.drawString("Your score:", 70, 350);
        g.setColor(Color.WHITE);

        g.drawString(Game_Score, 150, 450);
        g.setColor(Color.WHITE);
        g.drawString(Game_Score, 150, 450);
        g.setColor(Color.WHITE);
    }   // отрисовка GAME OVER

    public View(int x, int y, int SIDE){

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(x, y);
        setResizable(false);
        getContentPane().setBackground(Color.BLACK);
        Font f = new Font("Roboto", Font.BOLD, 20);
        setFont(f);
        this.SIDE=SIDE;
    } // Конструктор окна

}

